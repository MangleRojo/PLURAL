# PLURAL

![Plural portada](/src/images/Plural_port.jpg)

PLURAL es una investigación-creación que indaga sobre una forma alternativa del lenguaje enfocada en la comunicación multiespecie. En esta primera etapa del guion se busca explorar la evolución y creación de los personajes que dan forma a las ideas.

Algunos de los personajes son tres Gigantes Montaña que han permanecido dormidos durante mucho tiempo, Planet Press un equipo de noticias que busca cubrir todas las novedades en la galaxia, unos hongos vivientes que solo piensan en discutir o una serpiente tan larga que parece una gran carretera.


[Inicio](src/inicio.md)