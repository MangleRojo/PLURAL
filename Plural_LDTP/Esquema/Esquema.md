# Esquema

La manifestación ReVBuGs causa toda la linea de tiempo de los personajes, es una paradoja ya que este evento creará a los propios BuGs, y a muchos mas personajes que llegaron antes que ellos.

Planet Press, el único noticiero espacial, cubre todos los lugares y a todas aquellas noticias que hay que saber. Juan Rizo Proceso, Golphi y Mr. Coloide son el equipo de reportaje y transmisión. Obviamente no pueden viajar a todos los lugares solos sin un equipo especializado, y ahí es donde entra Hidra Tours, una linea de turismo interplanetario que cuenta con una amplia gama de ecosistemas para visitar.


Después de la ReVBuG todo se reinicia y muchos seres diferentes empiezan a salir ¡de todo tipo!

Algo se mueve en la maleza y gruñe… de repente, salta de la maleza y ¡es un Monstruo de Retamo! pequeño pero salvaje. Para descansar el monstruo de Retamo planta sus raíces en la tierra y descansa con su parche.

¡Cuando esta bestia despierte El Verjón tendrá que evacuar! Él es un monstruo gigante que está en un profundo sueño del cual no despertará en muchos siglos, tiene una gran boca pero no tiene ojos.

También esta otro gigante mucho mas amigable –y dormido–. En su ombligo tiene una casita amarilla. También, tiene rocas que hacen de ojos, boca y nariz; así que lo llamamos el Gigante Piedra.  

Todo El Verjón tiene una largaaaaa carretera, pero ¿y si te digo que esa carretera tiene…  ¡VIDA!? pues si, es una carretera serpiente. A veces, sueña con devorar un gran camión.

¿Que pasaría si una especie de civilización de abejorros de otro mundo cayera en la tierra? pues esos serian Figua y Catoro unos abejorros mutados, los Figua son mas pequeños 'tipo moscas' y los Catoro son como unos 'abejorros toro' con grandes cuernos.

Bombus, un abejorro de El Verjón, que es un pelín gordito.… es amarillo y tiene grandes ojos. Es fanático del néctar que guarda en pequeñas vasijas, vive en cuevas y revolotea buscando flores.

La guerra es algo duro, hay muchos muertos y heridos, se destruyen hogares, se muere todo lo que tenga que ver con la guerra, si… es dura la guerra de los ¡¡Monstruos Hongo!! Ellos pelean por cualquier cosa, ya sea por espacio para su hogar, o por una simple ramita, no tienen un mínimo de paciencia o paz, son furiosos, lo único que adoran es discutir.

¿Recuerdan al monstruo gigante? pues después de un tiempo le creció un árbol en la cabeza, no sabemos muy bien pero al parecer eso cambio su forma de ver al mundo, hoy es mas amigable y feliz, no desea destruir al mundo, el es El Gigante Árbol.

Planet Press no tuvo buenos comienzos, no tenían suficiente presupuesto para pagar todo lo necesario para el equipamiento y movilidad, pero a contra viento y marea entrevistaron a Hidra en un planeta deshabitado y vacío, solo con algunos elementos de la vida regados por ahí, después de eso todo mejoró.

Hidra Tours: la mejor opción (y única) para viajar por el multiverso. La fundadora Hidra es conocida por lograr cosas de la nada, apoya a Planet Pres con equipamientos y movilidad, que era lo que les faltaba, y con un poco de dinero, de vez en cuando. Ella es la mayor emprendedora de todas. ¡¡¡No olvides comprar tus Tickets ya!!!

Ese planeta con los elementos de la vida regados por ahí es el planeta simbiótico. En este momento no se a hallado ningún tipo de vida, hace un tiempo se envío una tripulación de células a explorar el planeta, pero no se registro que volvieran…

Los NPCs están conformados por: Jonh Horton Conway, David Greaber, James Lovelock, Lynn Margulis, Donna Haraway, Ursula K. Le Guin, Giorgio Agamben, Hundertwasser, Wyslawa Szymborska y Mark Fisher. Todos ellos han hecho algo que nos ha ayudado a formar nuestra vision del mundo. 

No olvidar a sus compañeros: Hormiga (Conway), Oruga (Greaber), Daisys (Lovelock), Célula (Margulis), Pimoa Cathulu (Haraway), Bosque (Le Guin), Dispositivo (Agamben), Espiral (Hundertwasser), Piedra (Szymborska), Raro y Espeluznante (Fisher).

Ingenio es un libro con vida, tiene un gran ojo en el centro de la portada; su compañera inseparable es la llave, que a su vez también tiene un ojo. El Ingenio es una recopilación de todo lo que se ha hablado anteriormente y de lo que vendrá.

¡Oh, al parecer hay un nuevo gigante! parece un pantano pero no lo es, es más un bosque de un verde oscuro, tiene dos ojos y una boca medio cerrados; esos dos rasgos faciales le dan una cara siniestra y terrorífica, pero en realidad no es malo: es callado y siniestro. Él es el Gigante Bosque.

La piedra siempre esta de mal humor ¿por qué? te preguntarás, pues porque todo el mundo quiere entrar pero nadie entiende ¡¡¡¡¡¡¡que-no-hay-puertas!!!!!!! Ella tiene un pequeño liquen viviendo en su cabeza. Su buena amiga Szymborska es la única que la entiende. 

Los demonios de segunda especie no toleran los malos modales, ellos se consideran los expertos en esa materia –literalmente–, y como son de la segunda especie, pues… no solo viven en un plano universal sino que viven ¡¡¡¡¡¡en todos los planos multiversales!!!!!! 

Después de vivir salvajemente los monstruos hongo dejaron de ser nómadas y empezaron a construir aldeas para vivir mas 'pacíficamente' pero su espíritu de guerra no les dejo vivir así tanto tiempo, ahora, con una organización y planificación de guerra mas elaboradas, empezó la segunda era de los Monstruos Hongo, en la cual  ahora son Fungs.

La Serpiente Carretera cambió, sigue siendo muy largaaaa, ya no sueña con devorar un camión, de hecho, ahora es amiga de un camión. Si la vez siempre va con un pequeño camión rojo en cabeza, ella, de vez en cuando, se mueve pero muy muy lentamente.

Una gran lengua te ataca. Te duele porque es una lengua con espinas. ¡Es el nuevo y mejorado Monstruo de Retamo! Tiene una flor de color amarillo en su cabeza y, como puedes leer, tiene una gran lengua. Ahora está mas conectado con la tierra y con sus vecinos, es de un color verde claro y oscuro. Todo el mundo piensa que el retamo es un invasor ¡pero no lo es! Él respeta a las plantas que están a su alrededor.

Tres cuervos vuelan graznando ¡son las Weird Sisters! en su forma de cuervos. La traducción de "weird" es ahora "raro", pero antes la palabra "wyrd" (una posible pre-palabra del ingles a weird) significaba "destino", así que una posible traducción seria "hermanas del destino" algo que si encajaría con el papel que tienen en Macbeth.

Ahora, tanto el Gigante Árbol, el Gigante Piedra y el Gigante Bosque son mas cercanos, de hecho, son muy amigos. Ahora tienen mas cercas y carreteras aunque a su vez también tienen mas árboles y plantas.

Le encanta el banano, la llama la aventura y es hogareña. Si, esa es Anadia _bogotensis_, o para los amigos Anadia, le gusta mucho estar en rocas tomando el sol mañanero, tiene un color cafesoso con un poco de verde oscuro, son pequeñas y muy ágiles, con algunas escamas en forma de hexágonos.

zzzzzzzzz… ahora con música de fondo ¡BOMBUS! Vuela comunicando noticias de LABombus. va con unos audífonos de color gris, es de color medio rojizo inclinándose a lo anaranjado, con unas pequeñas alitas vuela surcando y zumbando los cielos en busca de flores.

Concolor es un liquen de colores amarillentos y verdosos, crece en la corteza de los árboles o encima de las rocas, y tiene formas alocadas. Es el liquen que esta encima de la piedra malhumorada. 

Mundano es un personaje algo misterioso, no se sabe mucho sobre él pero desde las investigaciones se ha hallado algunas cosas interesantes; como que le gustan las cosas terrenales y materiales, y también estar en la alta sociedad. Por eso mismo tienen una enemistad con los demonios de segunda especie.

Por otro lado, está Doméstico. Se sabe mucho más sobre él, por ejemplo que le encantan las labores domésticas, pero a la vez odia trabajar fuera de casa. Tiene muchas pero muchas mascotas. Una parte de la protesta BuG es en oposición a Mundano y a Doméstico.

Bien, ya hablamos del medio de comunicación de Planet Press, pero no hemos hablado a fondo de los gustos y personalidades del equipo a cargo, así que hablemos de ellos. Juan Riso Proceso amante de la buena cocina, le gusta pasar el tiempo libre viendo y haciendo diferentes recetas. Él, como era de esperarse, comparte la comida con todo el equipo. En cuanto a su personalidad es tranquilo y se encarga de entrevistar a todo tipo de seres.

A Golphi le encantan los videojuegos y los juegos de mesa. Es un fantasma de unos cuantos siglos, esta un poco chiflado pero siempre preciso con la cámara, ya que es el camarografo del equipo.

Mr. Coloide le fascina todo lo relacionado con la ciencia. Serio pero amigable, es el editor del programa.

Sube el telón la obra empieza los actores salen la gente ríe.

Los actores son:
La Piedra - Princesa puerta.
Gigante Árbol - El dragón.
El Monstruo de Retamo - El Mago.
Fungs - Los duendes mágicos.
El Gigante Roca - El Caballero.
La Serpiente Carretera - El caballo.
El Gigante Bosque - El Troll.

Los Gestos controlan un tipo de magia. Los Gestos han hallado Ángeles Fósiles enterrados. En su mayoría son gatos con largas túnicas de diferentes colores, pero no solo son gatos, pueden ser cualquier ser que te puedas imaginar.


CONTINUARA… 

